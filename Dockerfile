FROM debian:stable
RUN apt-get update && apt-get -y install wget vim
WORKDIR /home
RUN wget https://github.com/turtlecoin/violetminer/releases/download/v0.2.2/violetminer-linux-v0.2.2.tar.gz
RUN tar xf violetminer-linux-v0.2.2.tar.gz
RUN cd violetminer-linux-v0.2.2
RUN sed -i 's/TRTLv3HEhz8WUM1DW7kiutcXRrywWWysCJGiHrwNg5D2Dt1qzT6PmX1C71Jg1VvSumazVYT5zMzwz9YoquP6iLfFXXEiwP3bxoJ/$PUBLIC_VERUS_COIN_ADDRESS/' mine.sh